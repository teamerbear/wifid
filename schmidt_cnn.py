"""
This module implements the Convolutional Neural Network (CNN) implemented by
Schmidt et. al. to function as a Wireless Interference Identifier. It is built
with the Keras framework on top of TensorFlow.
"""

from copy import deepcopy

import cPickle

import data_utils

from keras import backend as K
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D
from keras.utils import np_utils

import matplotlib.pyplot as plt

import numpy as np
import os


def configure_random_seed():
    """
    Seeds the numpy random number generator.
    """
    np.random.seed()


def setup_reduced_model():
    """
    Builds the Reduced CNN model to use for Wireless Interference Identification.
    Each layer is documented in the code below.
    """
    # Tell Keras that we're using a TensorFlow backend. This means that a 2D
    # convolutional model expects input data to be of the form:
    #   [ rows, cols, channels ]
    # An example of channels would be RGB.
    K.set_image_dim_ordering('tf')

    # Start building the model.
    model = Sequential()

    # Layer 1: 2-dimensional convolutional layers with 8 feature maps
    #          (convolution kernels) of dimension 3 x 1. Input dimensions (for a
    #          single sample) are 128 x 2 x 1. Output dimensions are 8 x 126 x 2.
    #          The 8 comes from the 8 kernels and the 126 comes from convolving
    #          the 3 x 1 kernel with a row of 128 inputs. The very first and
    #          very last sample get eliminated in the convolution.
    model.add(
        Conv2D(
            8, (3, 1),
            input_shape=(128, 2, 1),
            data_format='channels_last',
            activation='relu'))

    # Layer 2: 2-dimensional convolutional layer with 16 feature maps
    #          (convolutional kernels) of dimension 3 x 2. Input dimensions are
    #          8 x 126 x 2 (same as the output of the previous layer). Output
    #          dimensions (after flattening) are 1984 x 1.
    model.add(Conv2D(16, (3, 2), activation='relu'))

    # 60% dropout
    model.add(Dropout(0.6))

    # Flatten for input to dense layers.
    model.add(Flatten())

    # Layer 3: A dense, fully connected layer of 64 neurons with ReLU activation.
    model.add(Dense(64, activation='relu'))

    # Another 60% dropout
    model.add(Dropout(0.6))

    # Layer 4: A dense, fully connected layer of 15 neurons (the number of
    #          labels) with SoftMax activation.
    model.add(Dense(15, activation='softmax'))

    # Compile the model.
    model.compile(
        loss='categorical_crossentropy',
        optimizer='adam',
        metrics=['accuracy'])
    model.summary()

    return model


def extract_data(test_frac, debug=False):
    """
    Reads the pickle files containing the I/Q samples and their labels and
    transforms the data into the dimensions expected by the CNN. Splits the data
    into training and test sets based on the input parameter test_frac.

    Arguments:
        test_frac:  The fraction of the samples to use for validation.

    Returns:
        The data arrays in the form [x_train, y_train, x_test, y_test].
    """
    # Path to the directory containing the pickled data.
    folder = os.path.join(os.path.abspath(os.getcwd()), 'datasets')
    if debug:
        folder = os.path.join(folder, 'debug')
    else:
        folder = os.path.join(folder, 'schmidt')

    # Load IQ data, for FFT data use 'fft' instead of 'iq'.
    print "Loading data from pickle files . . .\n"
    features, labels = data_utils.load_data(folder, 'fft')

    # Split data into training and test sets. 
    print "Splitting data into training/test sets . . .\n"
    f_train, l_train, f_test, l_test = data_utils.split_data(
        features, labels, validation_fraction=test_frac)

    # One-hot encode all the labels.
    new_train_shape = np.append(l_train.shape, 15)
    new_test_shape = np.append(l_test.shape, 15)

    l_train_1h = np.empty(new_train_shape, dtype=int)
    l_test_1h = np.empty(new_test_shape, dtype=int)
    for i in xrange(l_train_1h.shape[0]):
        for j in xrange(l_train_1h.shape[1]):
            for k in xrange(l_train_1h.shape[2]):
                l_train_1h[i][j][k] = np_utils.to_categorical(
                    l_train[i][j][k], num_classes=15)
            for k in xrange(l_test_1h.shape[2]):
                l_test_1h[i][j][k] = np_utils.to_categorical(
                    l_test[i][j][k], num_classes=15)

    # Transform the samples to have a "channels" dimension so Keras will accept
    # it as input. Channels would normally correspond to something like RGB, but
    # we are just using a "grayscale" image with a single channel.
    new_train_shape = np.append(f_train.shape, 1)
    new_test_shape = np.append(f_test.shape, 1)
    f_train = np.reshape(f_train, new_train_shape)
    f_test = np.reshape(f_test, new_test_shape)

    return f_train, l_train_1h, f_test, l_test_1h


def remove_metadata(x_in, y_in, x_test_in, y_test_in):
    """
    Collapses the first three dimensions of the data (technology class, SNR
    level, batch index) into a single dimension for input into the neural
    network.

    Arguments:
        x_in:       The samples to use for training the neural network.
        y_in:       The one-hot encoded vector of labels corresponding to the
                    training data.
        x_test_in:  The samples to use for validation.
        y_test_in:  The one-hot encoded vector of labels corresponding to the
                    test data.
    
    Returns:
        A list of the data arrays with metadata removed (or collapsed).
            [ x_train, y_train, x_test, y_test ]
    """
    train_data_len = x_in.shape[0] * x_in.shape[1] * x_in.shape[2]
    test_data_len = x_test_in.shape[0] * x_test_in.shape[1] * x_test_in.shape[
        2]
    features_train = np.empty((train_data_len, 128, 2, 1), dtype=float)
    labels_train = np.empty((train_data_len, 15), dtype=int)
    features_test = np.empty((test_data_len, 128, 2, 1), dtype=float)
    labels_test = np.empty((test_data_len, 15), dtype=int)

    data_ind = 0
    for i in xrange(x_in.shape[0]):
        for j in xrange(x_in.shape[1]):
            for k in xrange(x_in.shape[2]):
                features_train[data_ind] = deepcopy(x_in[i][j][k])
                labels_train[data_ind] = deepcopy(y_in[i][j][k])
                data_ind += 1

    data_ind = 0
    for i in xrange(x_test_in.shape[0]):
        for j in xrange(x_test_in.shape[1]):
            for k in xrange(x_test_in.shape[2]):
                features_test[data_ind] = deepcopy(x_test_in[i][j][k])
                labels_test[data_ind] = deepcopy(y_test_in[i][j][k])
                data_ind += 1

    return features_train, labels_train, features_test, labels_test


def store_train_test_sets(x_train, y_train, x_test, y_test, path_prefix):
    """
    Stores the extracted training and test data sets used for training a model
    that we've saved. Stores the data in the Python pickle format. The format of
    the data itself that is passed into this function should be exactly the same
    as the original data set. This means that the same data processing that is
    required for the original data is also required for the data stored by this
    function upon loading.

    Arguments:
        x_train:        The dataset of training features.
        y_train:        The dataset of training labels.
        x_test:         The dataset of validation features.
        y_test:         The dataset of validation labels.
        path_prefix:    The path (including the base filename) where the data
                        should be stored, but without a file extension. The
                        function will append '_x_train.p' and '_y_train.p' for
                        the training sets and, similarly, '_x_test.p' and
                        '_y_test.p' for the test sets.
    """
    # Get the file names for each dataset.
    x_train_file = path_prefix + "_x_train.p"
    y_train_file = path_prefix + "_y_train.p"
    x_test_file = path_prefix + "_x_test.p"
    y_test_file = path_prefix + "_y_test.p"

    # Serialize the data and write it to the files.
    print "\n  Storing datasets . . ."
    print "    Storing {}".format(x_train_file)
    with open(x_train_file, 'w') as outfile:
        cPickle.dump(x_train, outfile)
    print "    Storing {}".format(y_train_file)
    with open(y_train_file, 'w') as outfile:
        cPickle.dump(y_train, outfile)
    print "    Storing {}".format(x_test_file)
    with open(x_test_file, 'w') as outfile:
        cPickle.dump(x_test, outfile)
    print "    Storing {}".format(y_test_file)
    with open(y_test_file, 'w') as outfile:
        cPickle.dump(y_test, outfile)
    print "  Done.\n"


def load_train_test_sets(path_prefix):
    """
    In the case that we're loading a saved model, loads the training and test
    sets (which have been stored in the Python pickle format) that were already
    extracted for training and validating that model. It should be noted that
    the stored data is in exactly the same format as was the original data, just
    chopped up into different sets for training and validation. This means that
    the same data processing that was required on the original dataset is also
    required here.

    Arguments:
        path_prefix:    The path (including the base filename) where the data
                        is stored, but without the file extension. The function
                        will append '_x_train.p' and '_y_train.p' for the
                        training sets and, similarly, '_x_test.p' and
                        '_y_test.p' for the test sets.
    Returns:
        A list of the training and test sets in the form:
            [ x_train, y_train, x_test, y_test ]
    """
    # Get the file names for each dataset.
    x_train_file = path_prefix + "_x_train.p"
    y_train_file = path_prefix + "_y_train.p"
    x_test_file = path_prefix + "_x_test.p"
    y_test_file = path_prefix + "_y_test.p"

    # Read the datasets out of the files they are stored in.
    print "\n  Loading saved datasets . . ."
    print "    Loading {}".format(x_train_file)
    with open(x_train_file, 'r') as infile:
        x_train = cPickle.load(infile)
    print "    Loading {}".format(y_train_file)
    with open(y_train_file, 'r') as infile:
        y_train = cPickle.load(infile)
    print "    Loading {}".format(x_test_file)
    with open(x_test_file, 'r') as infile:
        x_test = cPickle.load(infile)
    print "    Loading {}".format(y_test_file)
    with open(y_test_file, 'r') as infile:
        y_test = cPickle.load(infile)
    print "  Done.\n"

    return [x_train, y_train, x_test, y_test]


def train_model(model, x_train, y_train):
    """
    Trains the given model with the wireless data generated by Schmidt et. al.

    Arguments:
        model:      The built and compiled CNN model to train on the data.
        x_train:    The input features on which to train the CNN model.
        y_train:    The labels corresponding to x_train.
    """
    model.fit(x_train, y_train, epochs=200, batch_size=1024)


def evaluate_model(model, x_test, y_test):
    """
    Runs test data through a trained model to evaluate the performance of that
    model.

    Arguments:
        model:  The trained CNN model to evaluate.
        x_test: The data on which to evaluate the performance of the trained
                model.
        y_test: The labels corresponding to x_test.
    
    Returns:
        The accuracy of the trained model given the test data.
    """
    metrics = model.evaluate(x=x_test, y=y_test, verbose=1)
    return metrics


def get_acc_vs_snr(clf, x_data, y_data):
    """
    Evaluates the accuracy of the trained CNN against the test data for a
    particular technology class. Evaluates the accuracy at each SNR.
    
    Arguments:
        clf:        The trained CNN to evaluate.
        x_data:     The test features with the SNR metadata still included.
        y_data:     The test labels with the SNR metadata still included.
    
    Returns:
        A numpy array with the accuracy of the model at each SNR.
    """
    accs = np.zeros(21, dtype=float)

    for i in xrange(accs.shape[0]):
        [loss, accuracy] = evaluate_model(model, x_data[i], y_data[i])
        accs[i] = accuracy

    return accs


def create_accuracy_plot(accs, plot_title, filename):
    """
    Creates a plot of accuracy vs. SNR.

    Arguments:
        accs:           The array of accuracy for each SNR for the given
                        technology class.
        plot_title:     The desired title of the generated figure.
        filename:       The file where the graphic will be stored.
    """
    snr_axis = np.arange(-20, 21, 2, dtype=int)

    fig = plt.figure()
    plt.xlabel("SNR (dB)")
    plt.ylabel("Accuracy")
    plt.ylim((0.0, 1.0))
    plt.title(plot_title)
    plt.grid(True, linestyle='--')
    plt.plot(
        snr_axis,
        accs,
        linewidth=2,
        color=(192.0 / 256.0, 57.0 / 256.0, 43.0 / 256.0))
    fig.savefig(filename)


if __name__ == "__main__":

    # Get the data.
    configure_random_seed()

    # Allocate some variables
    full_x_train = None
    full_y_train = None
    full_x_test = None
    full_y_test = None

    x_train = None
    y_train = None
    x_test = None
    y_test = None

    # Create or retrieve the model.
    model = None
    cwd = os.path.abspath(os.getcwd())
    filepath = os.path.join(cwd, 'models', 'trained_cnn.h5')
    if os.path.exists(filepath):
        # If there is an already trained model, there should also be saved
        # datasets to go along with it.
        print "Found a trained model at \'{}\'. Skipping training.".format(
            filepath)
        full_x_train, full_y_train, full_x_test, full_y_test = load_train_test_sets(
            os.path.join(cwd, 'datasets', 'wifid'))
        x_train, y_train, x_test, y_test = remove_metadata(
            full_x_train, full_y_train, full_x_test, full_y_test)
        print "Num train samples = {}\nNum test samples = {}\n".format(
            x_train.shape[0], x_test.shape[0])

        print "\n\tLoading saved model . . .\n"
        model = load_model(filepath)
    else:
        full_x_train, full_y_train, full_x_test, full_y_test = extract_data(
            0.33, debug=False)
        store_train_test_sets(full_x_train, full_y_train, full_x_test,
                              full_y_test,
                              os.path.join(cwd, 'datasets', 'wifid'))
        x_train, y_train, x_test, y_test = remove_metadata(
            full_x_train, full_y_train, full_x_test, full_y_test)
        print "Num train samples = {}\nNum test samples = {}\n".format(
            x_train.shape[0], x_test.shape[0])

        model = setup_reduced_model()
        train_model(model, x_train, y_train)

        # Save our trained model so we don't have to constantly re-train.
        model.save(filepath)

    # Evaluate the overall accuracy of the model accross all SNRs (this metric
    # is probably not very useful).
    [loss, accuracy] = evaluate_model(model, x_test, y_test)
    print "\nOverall accuracy: {}\n".format(accuracy)

    # Create an array to store the accuracies we wish to report. Note that the
    # extra row is for averaging across all classes for each SNR.
    accuracies = np.empty((16, 21), dtype=float)

    # Create graphics of accuracy vs. SNR for each technology class.
    base_img_path = os.path.join(os.path.abspath(os.getcwd()), 'graphics', 'cnn')
    for i in xrange(accuracies.shape[0] - 1):
        accs = get_acc_vs_snr(model, full_x_test[i], full_y_test[i])
        accuracies[i] = deepcopy(accs)
        title = "Technology Class {}".format(i)
        img_path = os.path.join(base_img_path,
                                'cnn_class_{}_acc_vs_snr.png'.format(i))
        create_accuracy_plot(accs, title, img_path)
    
    # Now average accuracies accross all tech classes for each SNR.
    for i in xrange(accuracies.shape[1]):
        the_sum = 0.0
        for j in xrange(accuracies.shape[0] - 1):
            the_sum += accuracies[j][i]
        avg = the_sum / float(accuracies.shape[0] - 1)
        accuracies[accuracies.shape[0] - 1][i] = avg
    
    title = "Average Accuracies"
    img_path = os.path.join(base_img_path, 'cnn_avg_accs.png')
    create_accuracy_plot(accuracies[15], title, img_path)

    # Save the accuracies for reference.
    acc_file_path = os.path.join(os.path.abspath(os.getcwd()), "cnn_accs.csv")
    np.savetxt(acc_file_path, accuracies, delimiter=',', fmt='%.5f')

