"""
This module provides methods for easy data handling of the measurement data used
for training the CNN.

@author: Malte Schmidt (with additions by me)
"""
import os
import math
import matplotlib.pyplot as plt
import numpy as np
import pickle


def shuffle_in_unison(x_data, y_data, seed=195735):
    '''
	This method shuffles the data of the Data Set axis of the test and training data.
	It is inspired by http://stackoverflow.com/questions/4601373/better-way-to-shuffle-two-np-arrays-in-unison
	'''
    np.random.seed(seed)
    shuffled_x = np.empty(x_data.shape, dtype=x_data.dtype)
    shuffled_y = np.empty(y_data.shape, dtype=y_data.dtype)
    permutation = np.random.permutation(x_data.shape[2])
    for old_index, new_index in enumerate(permutation):
        shuffled_x[::, ::, new_index, ::, ::] = x_data[::, ::,
                                                       old_index, ::, ::]
        shuffled_y[::, ::, new_index] = y_data[::, ::, old_index]
    return shuffled_x, shuffled_y


def split_data(x_data, y_data, validation_fraction=0.2):
    """
	Splits the data into training and validation data
	according to the fraction that was specified. The samples are shuffled and then selected.
    The data is equally splitted along classes and signal to noise ratios.
	The new data array, validation array and the according label arrays are returned.
	"""
    # Shuffle data
    x_data, y_data = shuffle_in_unison(x_data, y_data)
    # Split data
    nb_sets = x_data.shape[2]
    nb_cutted = int(np.floor(nb_sets * validation_fraction))
    x_test = x_data[::, ::, -1:(-nb_cutted - 1):-1, ::, ::]
    y_test = y_data[::, ::, -1:(-nb_cutted - 1):-1]
    x_data = np.delete(x_data, np.s_[-1:(-nb_cutted - 1):-1], axis=2)
    y_data = np.delete(y_data, np.s_[-1:(-nb_cutted - 1):-1], axis=2)
    return x_data, y_data, x_test, y_test


def load_data(storage_folder, which_kind):
    """
	Unpickle the data stored in the folder. The name of the measurement data
	inside the folder is considered as 'data_arrays_$which_kind-data-' + $standards and the
	label data is considered as 'labels-data-' + $standards.
	The measurement data and the labels for each standard 
	are returned in a list.
	Standard must be a list of Strings. The data is loaded in the order of the
	standards.
	"""
    x_data = None
    y_data = None
    data_file = None
    # decide which data to load
    if which_kind == 'iq':
        data_file = 'data_iq.p'
    elif which_kind == 'fft':
        data_file = 'data_fft.p'
    else:
        raise ValueError(
            'Parameter which_kind must be "iq" for IQ-data or "fft" for FFT-data.'
        )
    # load input data (x)
    data_path = os.path.join(storage_folder, data_file)
    with open(data_path, mode='r') as storage:
        x_data = pickle.load(storage)
    # load output data/labels (y)
    label_file = 'labels.p'
    label_path = os.path.join(storage_folder, label_file)
    with open(label_path, mode='r') as storage:
        y_data = pickle.load(storage)
    return x_data, y_data


def normalize_data(x_train, x_test):
    """
	$x_train and $x_test are np arrays which should be normalized.
	Normalizes the training data to have a train_mean of 0 and a standard deviation of 1. 
	The test data is normalized with the parameters of the training data
	Returns the normalized data in the same format as given.
	"""
    train_mean = np.mean(x_train)
    train_std_dev = np.std(x_train)
    x_train = (x_train - train_mean) / train_std_dev  # element-wise operations
    x_test = (x_test - train_mean) / train_std_dev  # element-wise operations
    return x_train, x_test

def fft_plot(snapshot, class_index, snr_index, batch_index, filename):
    """
	Plots the spectrum (with power in dB) based on an FFT of the I/Q snapshot.

	Arguments:
		snapshot:		The snapshot of FFT-ed I/Q data.
		class_index:	The technology (label) to which the snapshot belongs.
						Values can range from 0 - 14.
		snr_index:		The index used to map into the SNR array. Index values
						range from 0 - 20 and those indices correspond to SNR
						values from -20 dB to 20 dB in steps of 2 dB.
		batch_index:	The index of the sample relative to the batch (class and
						SNR) the sample is in.
	"""
    # Grab several types of data that may be informative to plot.
    f_data = np.arange(2.395, 2.405, 0.01 / 128.0)  # 10 MHz sensing bandwidth
    mag_data = np.array([])
    power_data = np.array([])
    dB_data = np.array([])
    for iq in snapshot:
        power = iq[0] * iq[0] + iq[1] * iq[1]
        mag = math.sqrt(power)  # power is mangitude squared
        dB = 20.0 * math.log10(mag)  # convert to dB
        mag_data = np.append(mag_data, mag)
        power_data = np.append(power_data, power)
        dB_data = np.append(dB_data, dB)

    new_power_data = np.array([])
    new_mag_data = np.array([])
    new_dB_data = np.array([])

    # Re-arrange the data so it is centered.
    i = 64
    while i < 128:
        new_power_data = np.append(new_power_data, power_data[i])
        new_mag_data = np.append(new_mag_data, mag_data[i])
        new_dB_data = np.append(new_dB_data, dB_data[i])
        i += 1

    i = 0
    while i < 64:
        new_power_data = np.append(new_power_data, power_data[i])
        new_mag_data = np.append(new_mag_data, mag_data[i])
        new_dB_data = np.append(new_dB_data, dB_data[i])
        i += 1

    # Plot the data.
    snr = (snr_index * 2) - 20
    fig = plt.figure()
    plt.xlabel("Frequency (GHz)")
    plt.ylabel("Signal Magnitude (dB)")
    plt.title(
        "Sample FFT of Wireless Data\nTech Class = {}, SNR = {} dB, Sample {}".
        format(class_index, snr, batch_index))
    plt.grid(True, linestyle='--')
    plt.plot(
        f_data,
        new_dB_data,
        linestyle='-',
        linewidth=2,
        color=(25.0 / 256.0, 111.0 / 256.0, 61.0 / 256.0))
    fig.savefig(filename)
