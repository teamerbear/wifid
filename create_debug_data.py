"""
This script creates a 'debug dataset' consisting of 10 samples from each
technology class and SNR combination. The purpose of creating a very small
dataset is just to see that there are no logistical issues with the code.
"""

import copy
import data_utils
import numpy as np
import os
import cPickle
import time

if __name__ == "__main__":
    # Check to see if our job has already been done.
    if not (os.path.isdir(os.path.join(os.getcwd(), 'datasets', 'debug'))):
        # Load the large data set.
        print "Loading original data set . . ."
        folder = '/Users/eswens13/Projects/Machine_Learning/cs6350/wifid_data/labeled-data'
        features, labels = data_utils.load_data(folder, 'fft')

        # Remember that the data is a 5-dimensional numpy array of the form:
        #   [ class index ][ SNR ][ snapshot index ][ I/Q sample index ][ I or Q ]
        num_classes = features.shape[0]
        num_SNR = features.shape[1]
        num_snapshots = features.shape[2]
        num_iq_samples = features.shape[3]
        iq_dim = features.shape[4]

        print "Creating data set . . ."

        # Set the location to store the output data.
        os.mkdir(os.path.join(os.getcwd(), 'datasets', 'debug'))
        output_data_file = os.path.join(os.getcwd(), 'datasets', 'debug',
                                        'data_fft.p')
        output_labels_file = os.path.join(os.getcwd(), 'datasets', 'debug',
                                          'labels.p')

        # Allocate the memory for the data. Take ten snapshots from each class
        # and SNR combination.
        shape = (num_classes, num_SNR, 10, num_iq_samples, iq_dim)
        dt = np.float
        output_data = np.empty(shape, dtype=dt)

        shape = (num_classes, num_SNR, 10)
        output_labels = np.empty(shape, dtype=dt)

        # Loop through the dataset and randomly take samples to obtain our data.
        np.random.seed(int(time.time()))
        for tech_class in xrange(num_classes):
            for snr_ind in xrange(num_SNR):
                for i in xrange(10):
                    snapshot_ind = np.random.randint(0, high=num_snapshots)
                    output_data[tech_class][snr_ind][i] = copy.deepcopy(
                        features[tech_class][snr_ind][snapshot_ind])
                    output_labels[tech_class][snr_ind][i] = tech_class

        # Now that we have created the data arrays, we need to serialize them
        # and store them in pickle form. Use the same naming conventions that
        # the data_utils script expects.
        with open(output_data_file, 'w') as outfile:
            print "Serializing data set to file . . ."
            cPickle.dump(output_data, outfile)
        with open(output_labels_file, 'w') as outfile:
            print "Serializing labels to file . . ."
            cPickle.dump(output_labels, outfile)

    else:
        print "Debug data set already exists!\nPlease delete the \'datasets/debug\' " \
                + "folder and re-run this script if you want to generate a new dataset."
