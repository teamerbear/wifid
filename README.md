# Wireless Interference Identification (wifid)

This repo contains the code/models that will replicate the results of "Wireless
Interference Identification with Convolutional Neural Networks" by Schmidt et.
al.

The data set(s) used are the same as in the original paper and were kindly
shared by Dr. Malte Schmidt.

The data is serialized via the Python `pickle` module. After calling `load_data`
from the `data_utils` module, the un-pickled data is in the following format:

**Features: 5-dimensional numpy array**

    [ class index x SNR x snapshot index x I/Q sample index x Re or Im ]

The class index can take on values from 0 - 14. Each of these values corresponds to
a particular technology.  There is a very helpful diagram on page 3 of the paper
which diagrams visually the split between possible signals. This diagram shows the
third in a set of 8 classifiers and what signals could fall into the spectrum. This
classifier's center frequency is 2426.5 MHz.

Bluetooth divides the whole 2.4 GHz band into 80 1 MHz intervals. Thus, with our
sensing bandwidth of 10 MHz, we could be seeing Bluetooth in 10 different places.

The 802.15.4 standard divides the 2.4 GHz band into 16 different channels whose
centers are spaced by 5 MHz. Each channel has a bandwidth of 2 MHz. See:
https://www.researchgate.net/figure/IEEE-802154-channels-within-24Ghz-frequency-band_fig5_243893690
for a visual diagram.

The 802.11b/g standard allows for 14 different channels with 22 MHz bandwidth.
With such a bandwidth, these channels obviously overlap significantly. See:
https://en.wikipedia.org/wiki/IEEE_802.11b-1999 for a visual diagram.

The labels, then, can be assigned as follows. Each assignment is of the form
(technology, relative channel). See Table I in the paper for the relative
channel indexing scheme:

0:  IEEE 802.15.1, 0

1:  IEEE 802.15.1, 1

2:  IEEE 802.15.1, 2

3:  IEEE 802.15.1, 3

4:  IEEE 802.15.1, 4

5:  IEEE 802.15.1, 5

6:  IEEE 802.15.1, 6

7:  IEEE 802.15.1, 7

8:  IEEE 802.15.1, 8

9:  IEEE 802.15.1, 9

10:  IEEE 802.11b/g, 0

11:  IEEE 802.11b/g, 1

12:  IEEE 802.11b/g, 2

13:  IEEE 802.15.4, 0

14:  IEEE 802.15.4, 1

The SNR index can take on values from 0 - 20. Those values correspond to
SNR values from -20 dB to 20 dB in steps of 2.

The snapshot index is the index of the snapshot within it's particular
class/SNR batch.

The I/Q sample index can take on values from 0 - 127 since there are 128
I/Q samples in each snapshot.

The last index can be 0 (real part) or 1 (imaginary part).
