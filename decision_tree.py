"""
This module implements a Decision Tree classifier for the Wireless Interference
Identification problem.
"""

from copy import deepcopy
import cPickle
import data_utils
import matplotlib.pyplot as plt
import numpy as np
import os
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
import time


def prepare_data(f_train, l_train, f_test, l_test):
    """
    For input into a decision tree classifier, we need to flatten the data to be
    just a giant list of samples. We will flatten each single snapshot from a
    128 x 2 format to a 256 x 1 format and consider each separate I and Q value
    a different feature. The data being passed into this function is of the
    following form for features:
        [ tech class x SNR x snapshot x I/Q row x I or Q ]
    and the following form for labels:
        [ tech class x SNR x label]

    Arguments:
        f_train:    The raw training features extracted from the data file.
        l_train:    The raw training labels extracted from the data file.
        f_test:     The raw test features extracted from the data file.
        l_test:     The raw test labels extracted from the data file.
    
    Returns:
        Data arrays in the correct format for a decision tree classifier. For
        features:
            [ sample_num x feature_list ]
        And for labels:
            [ sample_num x label ]
    """
    # Get dimensions for the new training data.
    num_train_features = f_train.shape[0] * f_train.shape[1] * f_train.shape[2]
    x_train = np.empty((num_train_features, 256), dtype=float)
    y_train = np.empty(num_train_features, dtype=int)

    # Transfer the training data to the new arrays.
    ind = 0
    for i in xrange(f_train.shape[0]):
        for j in xrange(f_train.shape[1]):
            for k in xrange(f_train.shape[2]):
                flat_snapshot = deepcopy(f_train[i][j][k]).flatten()
                x_train[ind] = flat_snapshot
                y_train[ind] = deepcopy(l_train[i][j][k])
                ind += 1

    # Get dimensions for the new test data.
    num_test_features = f_test.shape[0] * f_test.shape[1] * f_test.shape[2]
    x_test = np.empty((num_test_features, 256), dtype=float)
    y_test = np.empty(num_test_features, dtype=int)

    # Transfer the test data to the new arrays.
    ind = 0
    for i in xrange(f_test.shape[0]):
        for j in xrange(f_test.shape[1]):
            for k in xrange(f_test.shape[2]):
                flat_snapshot = deepcopy(f_test[i][j][k]).flatten()
                x_test[ind] = flat_snapshot
                y_test[ind] = deepcopy(l_test[i][j][k])
                ind += 1

    return [x_train, y_train, x_test, y_test]


def store_train_test_sets(x_train, y_train, x_test, y_test, path_prefix):
    """
    Stores the extracted training and test data sets used for training a model
    that we've saved. Stores the data in the Python pickle format. The format of
    the data itself that is passed into this function should be exactly the same
    as the original data set. This means that the same data processing that is
    required for the original data is also required for the data stored by this
    function upon loading.

    Arguments:
        x_train:        The dataset of training features.
        y_train:        The dataset of training labels.
        x_test:         The dataset of validation features.
        y_test:         The dataset of validation labels.
        path_prefix:    The path (including the base filename) where the data
                        should be stored, but without a file extension. The
                        function will append '_x_train.p' and '_y_train.p' for
                        the training sets and, similarly, '_x_test.p' and
                        '_y_test.p' for the test sets.
    """
    # Get the file names for each dataset.
    x_train_file = path_prefix + "_x_train.p"
    y_train_file = path_prefix + "_y_train.p"
    x_test_file = path_prefix + "_x_test.p"
    y_test_file = path_prefix + "_y_test.p"

    # Serialize the data and write it to the files.
    print "\n  Storing datasets . . ."
    print "    Storing {}".format(x_train_file)
    with open(x_train_file, 'w') as outfile:
        cPickle.dump(x_train, outfile)
    print "    Storing {}".format(y_train_file)
    with open(y_train_file, 'w') as outfile:
        cPickle.dump(y_train, outfile)
    print "    Storing {}".format(x_test_file)
    with open(x_test_file, 'w') as outfile:
        cPickle.dump(x_test, outfile)
    print "    Storing {}".format(y_test_file)
    with open(y_test_file, 'w') as outfile:
        cPickle.dump(y_test, outfile)
    print "  Done.\n"


def load_train_test_sets(path_prefix):
    """
    In the case that we're loading a saved model, loads the training and test
    sets (which have been stored in the Python pickle format) that were already
    extracted for training and validating that model. It should be noted that
    the stored data is in exactly the same format as was the original data, just
    chopped up into different sets for training and validation. This means that
    the same data processing that was required on the original dataset is also
    required here.

    Arguments:
        path_prefix:    The path (including the base filename) where the data
                        is stored, but without the file extension. The function
                        will append '_x_train.p' and '_y_train.p' for the
                        training sets and, similarly, '_x_test.p' and
                        '_y_test.p' for the test sets.
    Returns:
        A list of the training and test sets in the form:
            [ x_train, y_train, x_test, y_test ]
    """
    # Get the file names for each dataset.
    x_train_file = path_prefix + "_x_train.p"
    y_train_file = path_prefix + "_y_train.p"
    x_test_file = path_prefix + "_x_test.p"
    y_test_file = path_prefix + "_y_test.p"

    # Read the datasets out of the files they are stored in.
    print "\n  Loading saved datasets . . ."
    print "    Loading {}".format(x_train_file)
    with open(x_train_file, 'r') as infile:
        x_train = cPickle.load(infile)
    print "    Loading {}".format(y_train_file)
    with open(y_train_file, 'r') as infile:
        y_train = cPickle.load(infile)
    print "    Loading {}".format(x_test_file)
    with open(x_test_file, 'r') as infile:
        x_test = cPickle.load(infile)
    print "    Loading {}".format(y_test_file)
    with open(y_test_file, 'r') as infile:
        y_test = cPickle.load(infile)
    print "  Done.\n"

    return [x_train, y_train, x_test, y_test]


def create_model(depth=None):
    """
    Creates a decision tree classifer for use in classifying wireless
    interference.

    Arguments:
        depth:  The maximum depth to allow the tree to grow (prevents
                overfitting to the training data).

    Returns:
        The classifier object
    """
    clf = None
    if depth is None:
        clf = DecisionTreeClassifier()
    else:
        clf = DecisionTreeClassifier(max_depth=depth)

    return clf


def train_model(clf, x_train, y_train):
    """
    Trains the given model based on the training data provided.

    Arguments:
        clf:        The classifier to train.
        x_train:    The features to use in training the classifier.
        y_train:    The labels to use in training the classifier.
    """
    print "\nTraining decision tree classifier . . ."
    start_time = time.time()
    clf.fit(x_train, y_train)
    end_time = time.time()
    print "  Done.\nTraining took {} seconds.".format(end_time - start_time)


def store_model(clf, filepath):
    """
    Stores a trained decision tree classifier in the pickle format in order to
    facilitate quick prediction without haveing to re-train a model.

    Arguments:
        clf:        The classifier to serialize and store.
        filepath:   The path where the trained classifier will be serialized and
                    stored.
    """
    print "\nStoring trained decision tree classifier at {}".format(filepath)
    with open(filepath, 'w') as outfile:
        cPickle.dump(clf, outfile)


def load_model(filepath):
    """
    Loads a pre-trained model that has been serialized and stored in Python
    pickle format.

    Arguments:
        filepath:   The path to the stored model.
    
    Returns:
        The trained decision tree classifier object.
    """
    print "\nLoading pre-trained model from {}".format(filepath)
    clf = None
    with open(filepath, 'r') as infile:
        clf = cPickle.load(infile)

    return clf


def get_acc_vs_snr(clf, x_data, y_data):
    """
    Evaluates the accuracy of the trained decision tree against the test data
    for a particular technology class. Evaluates the accuracy at each SNR.
    
    Arguments:
        clf:        The trained decision tree to evaluate.
        x_data:     The test features with the SNR metadata still included.
        y_data:     The test labels with the SNR metadata still included.
    
    Returns:
        A numpy array with the accuracy of the model at each SNR.
    """
    accs = np.zeros(21, dtype=float)

    for i in xrange(accs.shape[0]):
        flat_x = np.reshape(deepcopy(x_data[i]), (x_data[i].shape[0], 256))
        predictions = clf.predict(flat_x)
        accuracy = accuracy_score(y_data[i], predictions)
        accs[i] = accuracy

    return accs


def create_accuracy_plot(accs, plot_title, filename):
    """
    Creates a plot of accuracy vs. SNR for a particular technology class.

    Arguments:
        accs:           The array of accuracy for each SNR for the given
                        technology class.
        plot_title:     The desired title of the generated figure.
        filename:       The file where the graphic will be stored.
    """
    snr_axis = np.arange(-20, 21, 2, dtype=int)

    fig = plt.figure()
    plt.xlabel("SNR (dB)")
    plt.ylabel("Accuracy")
    plt.ylim((0.0, 1.0))
    plt.title(plot_title)
    plt.grid(True, linestyle='--')
    plt.plot(
        snr_axis,
        accs,
        linewidth=2,
        color=(52.0 / 256.0, 152.0 / 256.0, 219.0 / 256.0))
    fig.savefig(filename)


def create_tree_depth_graphic(accs, filename):
    """
    Creates a plot of the accuracy of several different decision tree models,
    each with different max depth settings.

    Arguments:
        accs:       An array of the accuracies for each decision tree model.
        filename:   The path to where the graphic should be stored.
    """
    snr_axis = np.arange(-20, 21, 2, dtype=int)

    fig = plt.figure()
    plt.xlabel("SNR (dB)")
    plt.ylabel("Accuracy")
    plt.ylim((0.0, 1.0))
    plt.title("Effect of Max Depth")
    plt.grid(True, linestyle='--')
    plt.plot(
        snr_axis,
        accs[0][15],
        linewidth=2,
        color=(192.0 / 256.0, 57.0 / 256.0, 43.0 / 256.0))  # Red
    plt.plot(
        snr_axis,
        accs[1][15],
        linewidth=2,
        color=(52.0 / 256.0, 152.0 / 256.0, 219.0 / 256.0))  # Blue
    plt.plot(
        snr_axis,
        accs[2][15],
        linewidth=2,
        color=(39.0 / 256.0, 174.0 / 256.0, 96.0 / 256.0))  # Green
    plt.plot(
        snr_axis,
        accs[3][15],
        linewidth=2,
        color=(142.0 / 256.0, 68.0 / 256.0, 173.0 / 256.0))  # Purple
    plt.plot(
        snr_axis,
        accs[4][15],
        linewidth=2,
        color=(52.0 / 256.0, 73.0 / 256.0, 94.0 / 256.0))  # Gray
    plt.legend(
        [
            'Max Depth = 50', 'Max Depth = 100', 'Max Depth = 150',
            'Max Depth = 200', 'No Depth Limit'
        ],
        loc='lower right')
    fig.savefig(filename)


if __name__ == "__main__":

    # Allocate some variables.
    f_train = None
    l_train = None
    f_test = None
    l_test = None

    x_train = None
    y_train = None
    x_test = None
    y_test = None

    # Extract and prepare the data.
    data_path = os.path.join(os.path.abspath(os.getcwd()), 'datasets', 'dt')
    if not os.listdir(data_path):
        # If there is no pre-stored data, we need to create some.
        data_path = os.path.join(
            os.path.abspath(os.getcwd()), 'datasets', 'schmidt')
        print "Loading the data from {} . . .".format(data_path)
        features, labels = data_utils.load_data(data_path, 'fft')
        f_train, l_train, f_test, l_test = data_utils.split_data(
            features, labels, validation_fraction=0.33)
        data_path = os.path.join(
            os.path.abspath(os.getcwd()), 'datasets', 'dt', 'dt')
        store_train_test_sets(f_train, l_train, f_test, l_test, data_path)
    else:
        # If there is pre-existing data, load it.
        print "Loading the data from {} . . .".format(data_path)
        f_train, l_train, f_test, l_test = load_train_test_sets(
            os.path.join(data_path, 'dt'))

    # Create a plot for each signal type at high SNR.
    base_graphics_path = os.path.join(
        os.path.abspath(os.getcwd()), 'graphics', 'snapshots')
    for i in xrange(f_train.shape[0]):
        data_utils.fft_plot(
            f_train[i][-1][0], i, 20, 0,
            os.path.join(base_graphics_path, "class_{}.png".format(i)))

    #print "Reshaping data . . ."
    #x_train, y_train, x_test, y_test = prepare_data(f_train, l_train, f_test,
    #                                                l_test)

    #print "  Shape of x_train = {}".format(x_train.shape)
    #print "  Shape of y_train = {}".format(y_train.shape)
    #print "  Shape of x_test = {}".format(x_test.shape)
    #print "  Shape of y_test = {}".format(y_test.shape)

    ## Check for a pre-trained model.
    #clf = None
    #clf_50 = None
    #clf_100 = None
    #clf_150 = None
    #clf_200 = None
    #model_base_path = os.path.join(os.path.abspath(os.getcwd()), 'models')
    #model_path = os.path.join(model_base_path, 'trained_dt.p')
    #if os.path.exists(model_path):
    #    # Load the pre-trained model(s).
    #    clf = load_model(model_path)
    #    clf_50 = load_model(
    #        os.path.join(model_base_path, 'train_dt_50_depth.p'))
    #    clf_100 = load_model(
    #        os.path.join(model_base_path, 'train_dt_100_depth.p'))
    #    clf_150 = load_model(
    #        os.path.join(model_base_path, 'train_dt_150_depth.p'))
    #    clf_200 = load_model(
    #        os.path.join(model_base_path, 'train_dt_200_depth.p'))
    #else:
    #    # If there are no pre-existing models, create the models and train them.
    #    clf = create_model()
    #    clf_50 = create_model(depth=50)
    #    clf_100 = create_model(depth=100)
    #    clf_150 = create_model(depth=150)
    #    clf_200 = create_model(depth=200)

    #    train_model(clf, x_train, y_train)
    #    train_model(clf_50, x_train, y_train)
    #    train_model(clf_100, x_train, y_train)
    #    train_model(clf_150, x_train, y_train)
    #    train_model(clf_200, x_train, y_train)

    #    store_model(clf, model_path)
    #    store_model(clf_50,
    #                os.path.join(model_base_path, 'trained_dt_50_depth.p'))
    #    store_model(clf_100,
    #                os.path.join(model_base_path, 'trained_dt_100_depth.p'))
    #    store_model(clf_150,
    #                os.path.join(model_base_path, 'trained_dt_150_depth.p'))
    #    store_model(clf_200,
    #                os.path.join(model_base_path, 'trained_dt_200_depth.p'))

    ## Evaluate the overall accuracy of the model (this is probably not a very
    ## useful metric).
    #predictions = clf.predict(x_test)
    #accuracy = accuracy_score(y_test, predictions)
    #print "Overall accuracy of the model is {}".format(accuracy)

    ## Create an array to store the accuracies we wish to report. Note that the
    ## extra row is for averaging across all classes for each SNR.
    #accuracies = np.empty((5, 16, 21), dtype=float)

    ## Create graphics of accuracy vs. SNR for each technology class.
    #base_img_path = os.path.join(
    #    os.path.abspath(os.getcwd()), 'graphics', 'dt')
    #depth_models = [clf_50, clf_100, clf_150, clf_200, clf]
    #for j in xrange(accuracies.shape[0]):
    #    for i in xrange(accuracies.shape[1] - 1):
    #        accs = get_acc_vs_snr(depth_models[j], f_test[i], l_test[i])
    #        accuracies[j][i] = deepcopy(accs)
    #        if j == 4:
    #            title = "Technology Class {}".format(i)
    #            img_path = os.path.join(base_img_path,
    #                                    'dt_class_{}_acc_vs_snr.png'.format(i))
    #            create_accuracy_plot(accs, title, img_path)

    #    # Now average accuracies accross all tech classes for each SNR.
    #    for i in xrange(accuracies.shape[2]):
    #        the_sum = 0.0
    #        for k in xrange(accuracies.shape[1] - 1):
    #            the_sum += accuracies[j][k][i]
    #        avg = the_sum / float(accuracies.shape[1] - 1)
    #        accuracies[j][accuracies.shape[1] - 1][i] = avg

    ## Create a graphic of average accuracy over all technology classes for each
    ## SNR using the best model.
    #title = "Average Accuracies"
    #img_path = os.path.join(base_img_path, 'dt_avg_accs.png')
    #create_accuracy_plot(accuracies[4][15], title, img_path)

    ## Create a graphic that demonstrates the effect of limiting the tree's depth.
    #create_tree_depth_graphic(accuracies,
    #                          os.path.join(base_img_path, 'max_depth.png'))

    ## Save the accuracies for reference.
    #acc_file_path = os.path.join(os.path.abspath(os.getcwd()), "dt_accs.csv")
    #np.savetxt(acc_file_path, accuracies[4], delimiter=',', fmt='%.5f')
