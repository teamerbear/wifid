"""
This module is intended to be used for any plotting utilities.
"""

import matplotlib.pyplot as plt
import numpy as np
import os

def generate_avg_comparison_plot(cnn_accs, dt_accs, plot_title, filename):
    """
    Generates a plot with average accuracies for both the CNN and the decision
    tree classifier.

    Arguments:
        cnn_accs:   The average accuracies of the CNN for each SNR.
        dt_accs:    The average accuracies of the decision tree classifier for
                    each SNR.
        plot_title: The desired title of the figure.
        filename:   The name of the file where the figure will be stored.
    """
    snr_axis = np.arange(-20, 21, 2, dtype=int)

    fig = plt.figure()
    plt.xlabel("SNR (dB)")
    plt.ylabel("Accuracy")
    plt.ylim((0.0, 1.0))
    plt.title(plot_title)
    plt.grid(True, linestyle='--')
    plt.plot(
        snr_axis,
        cnn_accs,
        linewidth=2,
        color=(192.0 / 256.0, 57.0 / 256.0, 43.0 / 256.0))
    plt.plot(
        snr_axis,
        dt_accs,
        linewidth=2,
        color=(52.0 / 256.0, 152.0 / 256.0, 219.0 / 256.0))
    plt.legend(['CNN', 'Decision Tree'], loc='lower right')
    fig.savefig(filename)


if __name__ == "__main__":

    # Extract the data from the accuracy files.
    base_path = os.path.abspath(os.getcwd())    
    dt_file = os.path.join(base_path, 'dt_accs.csv')
    cnn_file = os.path.join(base_path, 'cnn_accs.csv')

    dt_lines = []
    with open(dt_file, 'r') as infile:
        dt_lines = infile.readlines()

    cnn_lines = []
    with open(cnn_file, 'r') as infile:
        cnn_lines = infile.readlines()
    
    avg_dt = dt_lines[-1].strip().split(',')
    avg_cnn = cnn_lines[-1].strip().split(',')

    avg_dt = [float(x) for x in avg_dt]
    avg_cnn = [float(x) for x in avg_cnn]
    
    # Plot the two different accuracies together.
    title = "Average Accuracy Comparison"
    filename = os.path.join(base_path, 'graphics', 'avg_comparison.png')
    generate_avg_comparison_plot(avg_cnn, avg_dt, title, filename)
